var i;
function light() {
	document.bgColor="ffffff";
	document.fgColor="000000";
	document.getElementById("bar1").style.backgroundColor="000000";
	document.getElementById("bar2").style.backgroundColor="000000";
	document.getElementById("bar3").style.backgroundColor="000000";
	document.getElementById("sidebar1").style.backgroundColor="000000";
	document.getElementById("a1").style.color="ffffff";
	document.getElementById("a2").style.color="ffffff";
	document.getElementById("a3").style.color="ffffff";
	document.getElementById("a4").style.color="ffffff";
	document.getElementById("a5").style.color="ffffff";
	document.getElementById("a6").style.color="ffffff";
	document.getElementById("a7").style.color="ffffff";
	if (document.getElementById("table")) {
		for (i=0;i<24;i++) {
			document.getElementById("table").querySelectorAll("td")[i].style.borderColor="000000";
		}
	} else {
		null;
	}
        if (document.getElementById("mmbr")) {
                for (i=0;i<10;i++) {
                         document.getElementById("mmbr").querySelectorAll("td")[i].style.borderColor="000000";
                }
        } else {
                null;
        }
	if (document.getElementById("downloadd")){
		for (i=1;i<7;i++) {
			var txt="dwd"+i;
			document.getElementById(txt).style.color="000000";
		}
	} else {
		null;
	}
	if (document.getElementById("req")) {
		document.getElementById("loader").className="roller1";
		for (i=0;i<8;i++) {
			document.getElementById("req").querySelectorAll("input")[i].style.backgroundColor="000000";
			document.getElementById("req").querySelectorAll("input")[i].style.color="ffffff";
		}
	} else {
		null;
	}
	window.localStorage.removeItem("dark");
	window.localStorage.setItem("dark","light");
}
function darkm() {
	document.bgColor="000000";
	document.fgColor="ffffff";
	document.getElementById("bar1").style.backgroundColor="ffffff";
	document.getElementById("bar2").style.backgroundColor="ffffff";
	document.getElementById("bar3").style.backgroundColor="ffffff";
	document.getElementById("sidebar1").style.backgroundColor="ffffff";
	document.getElementById("a1").style.color="000000";
	document.getElementById("a2").style.color="000000";
	document.getElementById("a3").style.color="000000";
	document.getElementById("a4").style.color="000000";
	document.getElementById("a5").style.color="000000";
	document.getElementById("a6").style.color="000000";
	document.getElementById("a7").style.color="000000";
	if (document.getElementById("table")) {
		for (i=0;i<24;i++) {
			document.getElementById("table").querySelectorAll("td")[i].style.borderColor="ffffff";
		}
	} else {
		null;
	}
	if (document.getElementById("downloadd")) {
		for (i=1;i<7;i++) {
			var txt="dwd"+i;
			document.getElementById(txt).style.color="ffffff";
		}
	} else {
		null;
	}
        if (document.getElementById("mmbr")) {
                for (i=0;i<10;i++) {
                         document.getElementById("mmbr").querySelectorAll("td")[i].style.borderColor="ffffff";
                }
        } else {
                null;
        }
	if (document.getElementById("req")) {
		document.getElementById("loader").className="roller2";
		for (i=0;i<8;i++) {
			document.getElementById("req").querySelectorAll("input")[i].style.backgroundColor="ffffff";
			document.getElementById("req").querySelectorAll("input")[i].style.color="000000";
		}
	} else {
		null;
	}
	window.localStorage.removeItem("dark");
	window.localStorage.setItem("dark","night");
}
function dark(ele) {
	if (ele.checked) {
		darkm();
	} else {
		light();
	}
}
if (window.localStorage.getItem("dark")) {
	if (window.localStorage.getItem("dark") == "light") {
		light();
	} else {
		document.getElementById("darrk").checked=true;
		darkm();
	}
} else {
	light();
}
