var rev=ScrollReveal({reset:true});
var i=0;
rev.reveal('.head',{delay:200});
if (document.getElementById("table")) {
	rev.reveal(".about",{delay:400});
	rev.reveal(".logo",{delay:490});
	rev.reveal(".who",{delay:550});
	rev.reveal(".thead",{delay:500});
	rev.reveal(".mmbr",{delay:500});
	rev.reveal(".tbd",{delay:540});
} else {
	null;
}
if (document.getElementById("downloadd")) {
	var time=500;
	rev.reveal(".thead",{delay:580});
	for (i=1;i<8;i++) {
		var txt=".dnd"+i;
		rev.reveal(txt,{delay:time});
		time=time+15;
	}
} else {
	null;
}
if (document.getElementById("howto")) {
	rev.reveal(".thead",{delay:580});
	rev.reveal(".how",{delay:500});
} else {
	null;
}
